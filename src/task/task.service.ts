import { Injectable } from '@nestjs/common';
import {TaskEntity } from './task.entity'
import { InjectRepository  } from '@nestjs/typeorm'
import { Repository } from 'typeorm'
import { UpdateResult, DeleteResult } from 'typeorm'

@Injectable()
export class TaskService {
    constructor (
        @InjectRepository(TaskEntity)
    private readonly taskRepo: Repository<TaskEntity>
    ){}

    //Hien thi tat ca danh sach
    async findAll(): Promise<TaskEntity[]>{
        return await this.taskRepo.find(); 
    }

    //Tim kiem 1 phan tu
    async findOne(id: number): Promise<TaskEntity>{
        return await this.taskRepo.findOne(id);
    }

    // Tao 1 task moi
    async create(task : TaskEntity): Promise<TaskEntity>{
        return await this.taskRepo.save(task);
    }

    //Sua 1 Task
    async update(task: TaskEntity): Promise<UpdateResult>{
        return await this.taskRepo.update(task.id, task);
    }
    // Xoa 1 task
    async delete(id): Promise<DeleteResult>{
        return await this.taskRepo.delete(id);
    }
}
